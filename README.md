# Wordpress Gallery Slow Upload

#### What's this for?

This is a wordpress plugin created to solve a very specific issue with the Gutenberg editor.

If the Wordpress administration interface is behind a reverse proxy such as Cloudflare or AWS Cloudfront, oftentimes there is a request execution time limit that cannot be changed. Usually this is 60 seconds.

When an editor uploads a large amount of large images to a Gutenberg gallery block (common in photography sites), combined with a slow connection this could mean that individual requests go over 60 seconds. For example, 10 requests may be running concurrently each uploading at 100kbps, causing them to take >60 seconds to upload.

With this plugin, image uploads from the Gutenberg editor are queued to run with a maximum concurrency limit. For example, for the following image upload:

|                           |            |
|---------------------------|------------|
| Number of Images          | 100 images |
| Image size each           | 1mb        |
| Internet connection speed | 1mb/s      |

The request characteristics look like this:

|                                   | Gutenberg   | With Plugin (Concurrency = 5) |
|-----------------------------------|-------------|-------------------------------|
| Total time                        | 100 seconds | 100 seconds                   |
| Average time per request          | 100 seconds | 5 seconds                     |
| Average per-request upload speed  | 10kbps      | 200kbps                       |
| Max open requests at any one time | 100         | 5                             |

As you can see, the plugin doesn't improve the total time needed to perform the upload (that would be impossible) but rather speeds up individual uploads by running less at once.

As a side-benefit, this also reduces server load by keeping less connections open simeultaneously.

#### How do I install this? 

See readme.txt