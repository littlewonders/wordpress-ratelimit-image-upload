=== Wordpress Ratelimit Image Upload ===
Contributors:      Jay Williams <admin@jaywilliams.me>
Tags:              block
Tested up to:      6.1
Stable tag:        1.1.1
License:           GPL-2.0-or-later
License URI:       https://www.gnu.org/licenses/gpl-2.0.html

Ratelimit concurrent uploads 

== Description ==

See README.md

== Installation ==

1. Clone this repository and build the assets by running `npm install` then `npm run build`
2. Upload the plugin files to the `/wp-content/plugins/wordpress-ratelimit-image-upload` directory, or install the plugin through the WordPress plugins screen directly.
3. Activate the plugin through the 'Plugins' screen in WordPress