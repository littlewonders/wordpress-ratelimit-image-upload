import apiFetch from '@wordpress/api-fetch';
import PromiseQueue from 'promise-queue/lib/index';

const config = window.ratelimit_image_upload_options;

const maxConcurrent = config.concurrent;
const maxQueue = Infinity;
const queue = new PromiseQueue(maxConcurrent, maxQueue);

apiFetch.use(async (options, next) => {
	if (!options.method || options.method.toUpperCase() !== 'POST') return await next(options);
	return await queue.add(() => next(options));
});