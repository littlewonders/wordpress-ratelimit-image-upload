<?php

add_filter('plugin_action_links_'.plugin_basename(__DIR__.'/ratelimit-image-upload.php'), 'ratelimit_image_upload_settings_link');

function ratelimit_image_upload_settings_link($links)
{
    $links[] = '<a href="'.admin_url('options-general.php?page=ratelimit_image_upload_settings').'">'. esc_html('Settings','ratelimit_image_upload').'</a>';

    return $links;
}


add_action('admin_menu', 'ratelimit_image_upload_register_options');

function ratelimit_image_upload_register_options()
{
	add_options_page('Ratelimit Image Uploads', 'Ratelimit Image Uploads', 'manage_options', 'ratelimit_image_upload_settings', 'ratelimit_image_upload_options_content');
}

function ratelimit_image_upload_options_content()
{
?>
	<h2>Ratelimit Image Uploads</h2>
	<form method="post" action="options.php">
		<?php
		settings_fields('ratelimit_image_upload_settings');
		?>
		<table class="form-table">
			<tbody>
				<tr>
					<th>Concurrent Requests</th>
					<td>
						<input type="text" name="concurrent" id="concurrent" value="<?php echo get_option('concurrent') ?: '3'; ?>">
						<br>
						<span class="description">How many image uploads will be allowed at once (Default: 3).</span>
					</td>
				</tr>
			</tbody>
		</table>
		<?php
		submit_button();
		?>
	</form>
<?php
}

add_action('admin_init', 'ratelimit_image_upload_register');

function ratelimit_image_upload_register()
{
	register_setting('ratelimit_image_upload_settings', 'concurrent');
}
