<?php

/**
 * Plugin Name:       [LW] Ratelimit Image Upload
 * Description:       Stops too many images being uploaded at once, therefore working around 60 second upload timeouts
 * Requires at least: 5.0
 * Requires PHP:      7.0
 * Version:           1.1.1
 * Author:            Jay Williams <@admin@jaywilliams.me>
 * License:           GPL-2.0-or-later
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:       ratelimit_image_upload
 *
 * @package           create-block
 */

require_once __DIR__ . '/settings.php';

function ratelimit_image_upload_enqueue_block_editor_assets()
{

	$dir = dirname(__FILE__);
	$script_asset_path = "$dir/build/index.asset.php";
	if (!file_exists($script_asset_path)) {
		throw new Error(
			'You need to run `npm start` or `npm run build` for the block first.'
		);
	}
	$script_asset = require($script_asset_path);

	wp_enqueue_script(
		'ratelimit-image-upload',
		plugins_url('build/index.js', __FILE__),
		$script_asset['dependencies'],
		filemtime(plugin_dir_path(__FILE__) . 'build/index.js')
	);
	wp_add_inline_script('ratelimit-image-upload', 'var ratelimit_image_upload_options = ' . json_encode([
		"concurrent" => get_option('concurrent') ?: 3
	]), 'before');
}
add_action('enqueue_block_editor_assets', 'ratelimit_image_upload_enqueue_block_editor_assets');
